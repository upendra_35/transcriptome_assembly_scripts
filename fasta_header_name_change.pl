#!/usr/bin/perl
#Date:
#User:
#Use:
#use say;
use strict;
use warnings;

open(my $infile, "<", "test.fasta") or die "Error opening input file"; 
open(my $outfile, ">", "test_out.fasta") or die "Error creating output file"; 

my $count = 1;
while (my $line = <$infile>) {
	if ($line =~ /^>/) {
		printf $outfile ">Bra%.5d\n", $count;
		$count ++;
	}
	else {
		print $outfile $line;
	}
}
